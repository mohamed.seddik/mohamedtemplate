# MohamedTemplate

**LOCAL:**

- Angular : http://localhost:4200/*

- Spring Boot : http://localhost:8080/api/*

- Plugin Swagger : http://localhost:8080/swagger-ui.html#/

**DOCKER:**

- Angular : http://localhost:81/*

- Spring Boot : http://localhost:82/api/*

- Jenkins : http://localhost:8080/*
