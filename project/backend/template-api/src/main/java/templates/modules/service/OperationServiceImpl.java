package templates.modules.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import templates.modules.entity.Account;
import templates.modules.entity.Operation;
import templates.modules.repository.OperationRepository;

import java.util.List;
import java.util.Optional;

@Service
public class OperationServiceImpl implements OperationService{

    @Autowired
    private OperationRepository operationRepository;

    public Operation findById(Long id){
        Optional<Operation> operation = operationRepository.findById(id);
        if (operation.isPresent()){
            return operation.get();
        }   else{
            return null;
        }
    }

    @Override
    public Operation save(Operation operation) {
        return operationRepository.save(operation);
    }

    @Override
    public boolean checkOperationInfos(Operation operation) {
        return operation.getAccount()==null || operation.getAmount()<=0 ||
                operation.getType().equals(Operation.TYPE.WITHDRAW) &&
                        operation.getAccount().getBalance()<=operation.getAmount();
    }

    @Override
    public List<Operation> findOperationsByAccountId(Account account) {
        return operationRepository.findOperationsByAccountId(account.getId());
    }
}
