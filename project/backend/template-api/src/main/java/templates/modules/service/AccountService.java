package templates.modules.service;

import templates.modules.entity.Account;
import templates.modules.entity.Operation;

import java.util.List;

public interface AccountService {
    List<Account> findAll();

    Account findById(Long id);

    void updateBalance(Operation operation);

    Account createAccount(Account account);
}
