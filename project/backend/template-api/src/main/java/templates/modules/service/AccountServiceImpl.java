package templates.modules.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import templates.modules.entity.Operation;
import templates.modules.repository.AccountRepository;
import templates.modules.entity.Account;

import java.util.List;
import java.util.Optional;

@Service
public class AccountServiceImpl implements AccountService{

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public List<Account> findAll() {
        return accountRepository.findAll();
    }

    @Override
    public Account findById(Long id){
        Optional<Account> account = accountRepository.findById(id);
        if (account.isPresent()){
            return account.get();
        }   else{
            return null;
        }
    }

    @Override
    public void updateBalance(Operation operation) {
        Account account = operation.getAccount();
        Double balance = operation.getType().equals(Operation.TYPE.DEPOSIT)?account.getBalance() + operation.getAmount():account.getBalance() - operation.getAmount();
        account.setBalance(balance);
        accountRepository.save(account);
    }

    @Override
    public Account createAccount(Account account) {
        return accountRepository.save(account);
    }
}
