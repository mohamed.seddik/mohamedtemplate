package templates.modules.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import templates.modules.dto.AccountDto;
import templates.modules.entity.Account;
import templates.modules.service.AccountService;
import templates.modules.service.OperationService;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/accounts")
public class AccountController {
    @Autowired
    private AccountService accountService;

    @Autowired
    private OperationService operationService;

    public Account mapperFromDto(AccountDto accountDto) {
        Account account = new Account();
        account.setId(accountDto.getId());
        account.setDateCreation(accountDto.getDateCreation());
        account.setBalance(accountDto.getBalance());
        return account;
    }

    public AccountDto mapperToDto(Account account) {
        AccountDto accountDto = new AccountDto();
        accountDto.setId(account.getId());
        accountDto.setDateCreation(account.getDateCreation());
        accountDto.setBalance(account.getBalance());
        return accountDto;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity getAccountById(@PathVariable("id") Long id) {
        Account account = accountService.findById(id);
        if(account==null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(mapperToDto(account), HttpStatus.OK);
    }

    @GetMapping("")
    public ResponseEntity index() {
        return new ResponseEntity("Test Success", HttpStatus.OK);
    }
}
