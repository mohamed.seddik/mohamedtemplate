package templates.modules.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import templates.modules.entity.Operation;

import java.util.List;

public interface OperationRepository extends JpaRepository<Operation, Long> {
    List<Operation> findOperationsByAccountId(Long accountId);
}
