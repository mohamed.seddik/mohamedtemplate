package templates.modules.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

public class AccountDto {
    private Long id;

    private Date dateCreation;

    private Double balance;

    public AccountDto() {
    }

    public AccountDto(Long id, Date dateCreation, Double balance) {
        this.id = id;
        this.dateCreation = dateCreation;
        this.balance = balance;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }
}
