package templates.modules.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

public class OperationDto {

    private Long id;

    private Long accountId;

    private String type;

    private Double amount;

    private Date dateCreation;

    public OperationDto() {
    }

    public OperationDto(Long id, Long accountId, String type, Double amount, Date dateCreation) {
        this.id = id;
        this.accountId = accountId;
        this.type = type;
        this.amount = amount;
        this.dateCreation = dateCreation;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getId() {
        return id;
    }

    public Long getAccountId() {
        return accountId;
    }

    public String getType() {
        return type;
    }

    public Double getAmount() {
        return amount;
    }

    public Date getDateCreation() {
        return dateCreation;
    }
}
