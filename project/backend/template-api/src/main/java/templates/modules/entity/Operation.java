package templates.modules.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "APP_OPERATION", uniqueConstraints = @UniqueConstraint(columnNames = "OPERATION_ID"))
public class Operation {
    public enum TYPE {
        DEPOSIT("DEPOSIT"), WITHDRAW("WITHDRAW");
        protected String value;

        TYPE(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value;
        }
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "OPERATION_ID", unique = true, nullable = false)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "ACCOUNT_ID_FK")
    private Account account;

    @Enumerated(EnumType.STRING)
    @Column(name = "TYPE")
    private TYPE type;

    @Column(name = "AMOUNT")
    private Double amount;

    @Column(name = "DATE_CREATION")
    private Date dateCreation;

    public Operation() {
    }

    public Operation(Long id, Account account, TYPE type, Double amount, Date dateCreation) {
        this.id = id;
        this.account = account;
        this.type = type;
        this.amount = amount;
        this.dateCreation = dateCreation;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public TYPE getType() {
        return type;
    }

    public void setType(TYPE type) {
        this.type = type;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }
}
