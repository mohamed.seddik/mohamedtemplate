package templates.modules.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import templates.modules.dto.OperationDto;
import templates.modules.entity.Account;
import templates.modules.entity.Operation;
import templates.modules.service.AccountService;
import templates.modules.service.OperationService;

import javax.transaction.Transactional;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin({"http://localhost:4200", "http://localhost:81"})
@RequestMapping("/api/operations")
public class OperationController {
    @Autowired
    private AccountService accountService;

    @Autowired
    private OperationService operationService;

    public Operation mapperFromDto(OperationDto operationDto) {
        Operation operation = new Operation();
        operation.setId(operationDto.getId());
        operation.setAccount(operationDto.getAccountId() != null ? accountService.findById(operationDto.getAccountId()) : null);
        operation.setType(Operation.TYPE.valueOf(operationDto.getType()));
        operation.setAmount(operationDto.getAmount());
        operation.setDateCreation(operationDto.getDateCreation());
        return operation;
    }

    public OperationDto mapperToDto(Operation operation) {
        OperationDto operationDto = new OperationDto();
        operationDto.setId(operation.getId());
        operationDto.setAccountId(operation.getAccount() != null ? operation.getAccount().getId() : null);
        operationDto.setType(operation.getType().name());
        operationDto.setAmount(operation.getAmount());
        operationDto.setDateCreation(operation.getDateCreation());
        return operationDto;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @Transactional
    public ResponseEntity createOperation(@RequestBody OperationDto operationDto) {
        try {
            Operation operation = mapperFromDto(operationDto);
            if (operation.getId() != null) {
                return new ResponseEntity(HttpStatus.CONFLICT);
            }
            if (operationService.checkOperationInfos(operation)) {
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }
            operation.setDateCreation(new Date());
            operation = operationService.save(operation);
            accountService.updateBalance(operation);
            return new ResponseEntity(mapperToDto(operation), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity getUserNotifications(@RequestParam("accountId") Long accountId) {
        Account account = accountService.findById(accountId);
        if (account == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        List<Operation> list = operationService.findOperationsByAccountId(account);
        return new ResponseEntity(
                list.stream().sorted(Comparator.comparing(Operation::getDateCreation)).map(operation -> mapperToDto(operation)).collect(Collectors.toList())
                , HttpStatus.OK);
    }
}
