package templates.modules.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "APP_ACCOUNT", uniqueConstraints = @UniqueConstraint(columnNames = "ACCOUNT_ID"))
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ACCOUNT_ID", unique = true, nullable = false)
    private Long id;

    @Column(name = "DATE_CREATION")
    private Date dateCreation;

    @Column(name = "BALANCE")
    private Double balance;

    @OneToMany(mappedBy = "account", fetch = FetchType.LAZY, orphanRemoval=true, cascade=CascadeType.PERSIST)
    private Set<Operation> operations;

    public Account() {
    }

    public Account(Long id, Date dateCreation, Double balance) {
        this.id = id;
        this.dateCreation = dateCreation;
        this.balance = balance;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Set<Operation> getOperations() {
        return operations;
    }

    public void setOperations(Set<Operation> operations) {
        this.operations = operations;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", dateCreation=" + dateCreation +
                ", balance=" + balance +
                '}';
    }
}
