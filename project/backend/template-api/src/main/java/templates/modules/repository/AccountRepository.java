package templates.modules.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import templates.modules.entity.Account;

import java.util.List;

public interface AccountRepository extends JpaRepository<Account, Long> {
    List<Account> findAll();
}
