package templates.modules.service;

import templates.modules.entity.Account;
import templates.modules.entity.Operation;

import java.util.List;

public interface OperationService {
    Operation save(Operation operation);

    boolean checkOperationInfos(Operation operation);

    List<Operation> findOperationsByAccountId(Account account);
}
