package templates.springbootapplication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import templates.modules.entity.Account;
import templates.modules.repository.AccountRepository;
import templates.modules.service.AccountService;

import java.util.Date;
import java.util.List;

@SpringBootApplication
@ComponentScan("templates")
@EntityScan("templates.modules.entity")
@EnableJpaRepositories("templates.modules.repository")
@EnableTransactionManagement
public class Application implements CommandLineRunner {
    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Autowired
    private AccountService accountService;

    @Override
    public void run(String... args) throws Exception {
        LOGGER.info("StartApplication...");
        runJDBC();
    }

    private void runJDBC() {
        Account account = new Account(1l, new Date(), 0d);
        account = accountService.createAccount(account);
        LOGGER.info("Account crée : " + account.toString());
        List<Account> list = accountService.findAll();
        LOGGER.info("Nombre d'elements trouves : {}", list.size());
    }
}
