import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {AccountService} from "../services/account.service";
import {Account} from "../models/Account";
import {Operation} from "../models/Operation";
import {OperationService} from "../services/operation.service";
import {ToastrService} from "ngx-toastr";

declare var $: any;

@Component({
  selector: 'app-deposit',
  templateUrl: './deposit.component.html',
  styleUrls: ['./deposit.component.css']
})
export class DepositComponent implements OnInit {
  account: Account = new Account();
  operation: Operation = new Operation();

  constructor(private router: Router, private accountService: AccountService, private operationService: OperationService, private toastr: ToastrService) {
  }

  ngOnInit(): void {
    this.loadAccount();
  }

  backHome() {
    this.router.navigate(['']);
  }

  private loadAccount() {
    this.accountService.loadAccount().then(
      (data: any) => {
        this.account = data;
      });
  }

  createOperation() {
    $("#exampleModal").modal("hide");
    if (this.operation.amount <= 0) {
      this.toastr.error("Veuiller saisir une valeur supérieur à 0");
    } else {
      this.operation.type = 'DEPOSIT';
      this.operation.accountId = this.account.id;
      this.operationService.createOperation(this.operation).then((data: any) => {
        this.toastr.success('Operation crées avec succées');
        this.loadAccount();
      }).catch(
        (data: any) => {
          this.toastr.error("Erreur est produite");
        }
      );
      this.operation= new Operation();
    }
  }
}
