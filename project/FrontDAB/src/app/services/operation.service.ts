import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Operation} from "../models/Operation";

@Injectable({
  providedIn: 'root'
})
export class OperationService {

  constructor(private http: HttpClient) { }

  createOperation(operation: Operation) {
    return this.http.post('/operations', operation).toPromise();
  }

  getOperationsByAccount(account: Account) {
    let params = new HttpParams();
    params = params.append('accountId', account.id);
    return this.http.get('/operations',{params: params}).toPromise();
  }
}
