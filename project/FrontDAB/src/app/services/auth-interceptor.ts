import {HTTP_INTERCEPTORS, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {environment} from "../../environments/environment";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  public BASE_URL = environment.apiUrl;

  constructor() {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {

    let authReq = req;
    authReq = req.clone({url: this.BASE_URL + req.url});
    return next.handle(authReq);
  }
}

export const httpInterceptorProviders = [
  {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}
];
