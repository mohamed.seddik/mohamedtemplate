import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(private http: HttpClient) { }

  loadAccount(accountId: any = 1) {
    return this.http.get('/accounts/' + accountId).toPromise();
  }

}
