export class Operation {
  constructor(
    public id : any = null,
    public accountId : any = null,
    public type : string = '',
    public amount : any = null,
    public dateCreation : any = null
  ){}
}
