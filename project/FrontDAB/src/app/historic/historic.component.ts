import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {Account} from "../models/Account";
import {AccountService} from "../services/account.service";
import {Operation} from "../models/Operation";
import {OperationService} from "../services/operation.service";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-historic',
  templateUrl: './historic.component.html',
  styleUrls: ['./historic.component.css']
})
export class HistoricComponent implements OnInit {
  account: Account = new Account();
  operations: Operation[];

  constructor(private router: Router, private accountService: AccountService, private operationService: OperationService, private toastr: ToastrService) {
    this.operations = new Array();
  }

  ngOnInit(): void {
    this.loadAccount();
  }

  backHome() {
    this.router.navigate(['']);
  }

  private loadAccount() {
    this.accountService.loadAccount().then(
      (data: any) => {
        this.account = data;
        this.loadOperations(this.account);
      }).catch(()=>{
      this.toastr.error("Erreur est produite dans la récupération du compte");
    });
  }

  private loadOperations(account : any) {
    this.operationService.getOperationsByAccount(account).then((operations: any) => {
      this.operations = operations;
    }).catch(()=>{
      this.toastr.error("Erreur est produite dans la récupération de l'historique des operations");
    })
  }
}
